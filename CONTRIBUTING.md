# /!\ WIP

# Contributing to this project

## Pull requests

1. Gitlab peoples, make sure that you enable [Repository Monitoring](https://docs.gitlab.com/ee/user/project/repository/repository_mirroring.html) on Gitlab.
2. Fork this project and clone your fork:
   ```
   git clone https://github.com/<your-username>/<repo-name>
   ```

## Tests
Test every rules you add by adding a fixture in the [test](`/tests`) directory.

* Add or remove a rule inside `/index.js`
* Write a fixture, with at least a `yep.scss` file.
* Run `yarn test` and make sure that the test return no errors.

### Writing a fixture
A fixture is just a simple directory with three files. One `scss` file that should fail and another one that should succeed, plus a `json` file with some informations :

```
. fixtures
└── fixture-name
    ├── check.json
    ├── nope.scss
    └── yep.scss
```

* `nope.scss` should <strong style="color: red;">**fail**</strong> and returns errors from stylelint.
* `yep.scss` should <strong style="color: green;">**not fail**</strong>. Full of beautiful and valid Sass formating with the Sass Guidelines.
* `check.json` contain informations about the numbers of `errors` the test on `nope.scss` should return and an array with the [rules](https://stylelint.io/user-guide/rules/list) violated. **Note**: _If no `check.json` is found, the test will assume that there's only a `yep.scss` file to test and will not care about `nope.scss` at all, even if there is one._

  Exemple of `check.json`:
  ```
  {
    "errors": 4,
    "rules": [
      "comment-whitespace-inside"
    ]
  }

  ```
