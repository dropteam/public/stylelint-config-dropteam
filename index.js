'use strict'

module.exports = {
  extends: [
    'stylelint-prettier/recommended',
    'stylelint-config-sass-guidelines'
  ],
  rules: {
    'comment-whitespace-inside': 'always',
    'selector-pseudo-element-colon-notation': 'single',
    'declaration-property-unit-whitelist': {
      'line-height': []
    },
    'at-rule-no-unknown': [
      true,
      {
        ignoreAtRules: [
          'function',
          'if',
          'each',
          'include',
          'mixin',
          'return',
          'else',
          'warn',
          'for'
        ]
      }
    ],
    'font-weight-notation': [
      'numeric',
      {
        ignore: ['relative']
      }
    ],
    'block-closing-brace-newline-after': [
      'always',
      {
        ignoreAtRules: ['if', 'else', 'else-if']
      }
    ],
    'at-rule-empty-line-before': [
      'always',
      {
        except: ['blockless-after-same-name-blockless'],
        ignore: ['after-comment', 'first-nested'],
        ignoreAtRules: ['if', 'else', 'else-if'],
        severity: 'warning'
      }
    ],
    'custom-property-empty-line-before': [
      'always',
      {
        except: ['after-custom-property'],
        ignore: ['after-comment', 'first-nested', 'inside-single-line-block'],
        severity: 'warning'
      }
    ],
    'declaration-empty-line-before': [
      'always',
      {
        except: ['after-declaration'],
        ignore: ['after-comment', 'first-nested', 'inside-single-line-block'],
        severity: 'warning'
      }
    ],
    'rule-empty-line-before': [
      'always',
      {
        ignore: ['after-comment', 'first-nested'],
        severity: 'warning'
      }
    ]
  }
}
