const stylelint = require('stylelint')
const path = require('path')
const glob = require('glob')
const fs = require('fs-extra')

const config = require('../')
const fixturesDir = path.resolve(__dirname, 'fixtures')

function runStylelint(fixturePath, file) {
  return stylelint.lint({
    files: path.join(fixturePath, file),
    config
  })
}

function createCorrectStyleCheck(fixturePath) {
  describe('correct style', () => {
    let result

    beforeAll(() => {
      result = runStylelint(fixturePath, 'yep.scss')
    })

    test('flags no warnings', async () => {
      expect.hasAssertions()
      expect.assertions(2)

      const data = await result

      expect(data.results[0].warnings).toHaveLength(0)
      expect(data.errored).toBeFalsy()
    }, 15000)
  })
}

function createIncorrectStyleCheck(fixturePath, rule) {
  describe('incorrect style', () => {
    let result

    beforeAll(() => {
      result = runStylelint(fixturePath, 'nope.scss')
    })

    test('flags correct number of warnings', async () => {
      expect.hasAssertions()
      expect.assertions(2)

      const data = await result

      expect(data.results[0].warnings).toHaveLength(rule.errors)
      expect(data.errored).toBeTruthy()
    })

    test('flags correct rule warnings', async () => {
      expect.hasAssertions()
      expect.assertions(rule.rules.length)

      const data = await result
      const received = data.results[0].warnings.map(warning => warning.rule)

      for (const warning of rule.rules) {
        expect(received).toContain(warning)
      }
    })
  })
}

describe('stylelint-config-dropteam', () => {
  let fixtures = glob.sync(`${fixturesDir}/*`)
  fixtures.forEach(fixturePath => {
    let name = fixturePath.replace(fixturesDir, '')
    let ruleFile = path.join(fixturePath, `check.json`)
    let ruleExist = fs.existsSync(ruleFile)
    let rule = ruleExist ? fs.readJSONSync(ruleFile) : false

    describe(`feature ${name}`, () => {
      createCorrectStyleCheck(fixturePath)
      if (rule) {
        createIncorrectStyleCheck(fixturePath, rule)
      }
    })
  })
})
