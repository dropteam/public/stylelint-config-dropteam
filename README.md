# Dropteam Stylelint config

> A sharable stylelint config object for Dropteam's projects

## Install

```
yarn add --dev stylelint-config-dropteam
```

## Usage
Extend your stylelint [configuration](https://stylelint.io/user-guide/configure#extends) with this config. You can still overrides thoses rules in your own project.

```
{
  "extends": "stylelint-config-dropteam"
}
```

## Documentation

This configuration is currently based on thoses configurations :

* [Sass Guidelines Stylelint config](https://github.com/bjankord/stylelint-config-sass-guidelines) using [Sass Guidelines](https://sass-guidelin.es/)
* [Prettier Stylelint config](https://github.com/prettier/stylelint-config-prettier)

### Comments
* [comment-whitespace-inside](https://stylelint.io/user-guide/rules/comment-whitespace-inside) - There must always be whitespace inside the markers

### Selectors
* [selector-pseudo-element-colon-notation](https://stylelint.io/user-guide/rules/selector-pseudo-element-colon-notation#selector-pseudo-element-colon-notation) - Applicable pseudo-elements must always use the single colon notation.

### Units
* [declaration-property-unit-whitelist](https://stylelint.io/user-guide/rules/declaration-property-unit-whitelist#declaration-property-unit-whitelist) - Line-height must always be unitless. Use only number values, not length.
* [font-weight-notation](https://stylelint.io/user-guide/rules/font-weight-notation#font-weight-notation) - Font weight must always use numeric value.

### Contributing
If you want to contribute to this project, adding rules or remove them, make sure to read the [contribution documentation](/CONTRIBUTING.md) before.
